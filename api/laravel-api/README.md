## Post Features
- Store new post.
- Show specific post.
- Show all post with 15 as pagination.
- Update specific post.
- Destroy post

## Comment Features
- Store comment for a certain post.
- Show specific comment.
- Show all comment to a certain post.
- Update comment.
- Destroy comment.

## Tools Used
- JwtAuth 
- spatie/laravel-sluggable
- Laravel Framework 7.8.1
- PHP 7.2.28