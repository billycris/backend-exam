<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Http\Resources\Post as PostResource;
use App\Http\Resources\PostCollection;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new PostCollection(Post::all()->paginate(15)); 
    }


    /**
     * return specific post
     */
    public function show(Post $post)
    {
        return (new PostResource($post))
                ->response()
                ->setStatusCode(200);

    }
    /**
     * Store a newly created post.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->json()->all(), [
            'title' => 'required|string|max:255',
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        $post = Post::create([
                    'title' => $request->title,
                    'content' => $request->content,
                    'image' => $request->input('image',null),
                    'user_id' => auth()->user('api')->id
        ]);

        return (new PostResource($post))
                ->response()
                ->setStatusCode(201);
    }

    /**
     * Update Post.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post )
    {
        $validator = Validator::make($request->json()->all(), [
            'title' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        
        $post = Post::findOrFail($post->id);
        $post->title = $request->input('title');
        $post->content = $request->input('content');

        $post->save();


        return (new PostResource($post))
                ->response()
                ->setStatusCode(200);
    }

    /**
     * Remove Post.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        
        $post->delete();
        return response()->json([
            'status' => 'Record deleted successfully'
        ]);

    }
}
