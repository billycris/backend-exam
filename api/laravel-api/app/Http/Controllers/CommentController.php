<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Post;
use App\Http\Resources\PostCollection;
use App\Http\Resources\Comment as CommentResource;
use App\Http\Resources\CommentCollection;
use Illuminate\Support\Facades\Validator;


class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new CommentCollection(Comment::all()->paginate(15));
    }

    public function show(Post $post, Comment $comment)
    {
        return (new CommentResource($comment))
                ->response()
                ->setStatusCode(200);
    }

    /**
     * Store a newly created comment.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Post $post )
    {
        $validator = Validator::make($request->json()->all(), [
            'body' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
       
        $comment = Comment::create([
                    'body' => $request->body,
                    'commentable_type'=> 'App\\Post',
                    'commentable_id'=> 1,
                    'parent_id'=>$post->id,
                    'creator_id' => auth()->user('api')->id
        ]);

        return (new CommentResource($comment))
                ->response()
                ->setStatusCode(201);
    }

    /**
     * Update comment.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post, Comment $comment)
    {
        $validator = Validator::make($request->json()->all(), [
            'body' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        
        $comment->body = $request->input('body');
        $comment->save();

        return (new CommentResource($comment))
                ->response()
                ->setStatusCode(200);
    }

    /**
     * Remove Comment.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post, Comment $comment)
    {
        
        $comment->delete();

        return response()->json([
            'status' => 'Record deleted successfully'
        ]);

    }
}
