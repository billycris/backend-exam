<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// User Registration
Route::post('/register', 'UserController@register');
Route::post('/login', 'UserController@login');

// Post
Route::apiResource('posts', 'PostController');

// Comment
Route::apiResource('posts.comments', 'CommentController');

// This will authentucate a user
Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('user', 'UserController@getAuthenticatedUser');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
